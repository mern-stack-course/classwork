console.log("Inside product.js file");

//variable declaration -> resever space
var age;

//value assignment -> add value into that space
age = 2; //int

//variable use -> read value from that space
console.log("Age:", age);

//declare + assignment
var userName = "Karan"; //string

console.log("My Name is:", userName);

//declaration and use

var standard; //undefined

console.log("I study in:", standard);

console.log("used first and declared later", useFirstDeclareLater);
var useFirstDeclareLater = 5;
console.log("use after declartion:", useFirstDeclareLater);

console.log("random value:", 1234, "karan");

console.log("not declared variable:", xyz);

var num = 12; // number
num = "karan"; //string
console.log("this is just number:", num);

var num1 = "2";
var num2 = 2;
var num3 = 5;
console.log("sum:", num1 - num2 + num3);
