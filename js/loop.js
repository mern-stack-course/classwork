console.log(1);
console.log(2);
console.log(3);
console.log(4);
console.log(5);

// starting  -> 1
// Endpoint - 5
// step -> +1
console.log("--------For------------");
for (var i = 1; i <= 5; i++) {
  console.log(i);
}
console.log("--------While------------");
var j = 1;
while (j <= 5) {
  console.log(j);
  j++;
}

console.log("--------Do While------------");
var j = 1;
do {
  console.log(j);
  j++;
} while (j <= 5);

//print odd numbers between 1 to 10
console.log("--------Odd Numbers------------");
// var num = 4;
// if (num % 2 == 1) {
//   console.log("odd");
// }

for (let num = 1; num <= 10; num++) {
  if (num % 2 == 1) {
    console.log(num);
  }
}

console.log("number:", num);
