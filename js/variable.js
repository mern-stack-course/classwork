// var i = 0; //old key var -> hoisting, re-declarable, global scope
// let j = 0; //es6 -> no hoisting, can't redeclare, block scope
// const k = 0; //es6 -> no hoisting,can't redeclare, block scope
let k = 1;
console.log(k);
k = 0;
console.log(k);
