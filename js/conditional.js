var age = 15;

console.log("Age:", age);

if (age >= 18) {
  console.log("Eligible for vote");
} else {
  console.log("Not Eligible for vote");
}

console.log("End of program");

/* Driving licence */
// age < 16 -> non eligible for driving licence
// age >= 16 -> non-gear licence
// age >= 18 -> gear vechile licence

var drivingAge = 21;
console.log("Driving Age:", drivingAge);
// if (drivingAge < 16) {
//   console.log("Not Eliible for driving licence");
// } else {
//   if (drivingAge >= 18) {
//     console.log("Eligible for gear vechile licence");
//   } else {
//     console.log("Eligible for non gear vechile licence");
//   }
// }

// if (drivingAge < 16) {
//   console.log("Not Eliible for driving licence");
// } else if (drivingAge >= 18) {
//   console.log("Eligible for gear vechile licence");
// } else {
//   console.log("Eligible for non gear vechile licence");
// }

if (drivingAge >= 18) {
  console.log("Eliible for gear driving licence");
} else if (drivingAge >= 16 && drivingAge < 18) {
  console.log("Eligible for non gear vechile licence");
} else {
  console.log("Not Eliible for driving licence");
}

//Grade
studentGrade = "B";
//A = laptop
//B =  console.log("Laptop")
//C = notebooks
//D = Pen
//rest -> console.log("Pen")

// if (studentGrade == "A") {
//   console.log("Laptop");
// } else if (studentGrade == "B") {
//   console.log("Phone");
// } else if (studentGrade == "C") {
//   console.log("NoteBoks");
// } else if (studentGrade == "D") {
//   console.log("Pen");
// } else {
//   console.log("Better luck next time");
// }

switch (studentGrade) {
  case "A":
    console.log("Laptop");
    break;
  case "B":
    console.log("Phone");
    break;
  case "C":
    console.log("NoteBooks");
    break;
  case "D":
    console.log("Pen"); //document
    break;
  default:
    console.log("Better luck next time");
}

//js false values -> "", 0, undefined, null

var a = null;
if (a) {
  console.log("undefined is true");
} else {
  console.log("undefined is false");
}

//student grade B, && non-gear driving licence -> age >= 16 & age < 18
//loptop

var hasDrivingLicence = false;

if (studentGrade == "A" || (studentGrade == "B" && hasDrivingLicence)) {
  console.log("Laptop");
} else if (studentGrade == "B") {
  console.log("Phone");
} else if (studentGrade == "C") {
  console.log("NoteBoks");
} else if (studentGrade == "D") {
  console.log("Pen");
} else {
  console.log("Better luck next time");
}
